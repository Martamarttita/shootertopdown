﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemiesSpawner : MonoBehaviour
{
    public const string NAME = "EnemiesSpawner";

    private List<Enemy> enemiesList;
    private List<Vector3> spawnPoints;

    private int enemiesInScene;

    private List<int> listIndexesEnemiesDead;
    private int indexListEnemiesDead;
    private int currentIndexEnemy;

    // List used to generate next random spawn point
    private List<Vector3> randomSpawnPoints;

    void Awake()
    {
        enemiesList = new List<Enemy>();
        enemiesInScene = 0;

        listIndexesEnemiesDead = new List<int>();
        for (int i = 0; i < ConfigGame.MAX_ENEMIES; i++)
        {
            listIndexesEnemiesDead.Add(i);
        }
        indexListEnemiesDead = 0;
        currentIndexEnemy = listIndexesEnemiesDead[indexListEnemiesDead];
    }


    private void FillIndexesRandomSpawnPoints()
    {
        randomSpawnPoints = new List<Vector3>();
        for (int i = 0; i < spawnPoints.Count; i++)
        {
            randomSpawnPoints.Add(spawnPoints[i]);
        }
    }


    public void SetAttrs(List<Vector3> spawnPoints, Player player, Action<int> enemyDie)
    {
        this.spawnPoints = spawnPoints;

        GameObject go = Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS + ValuesInfo.ENEMY_NAME);
        GameObject enemyGO;
        Enemy enemyBehav;

        for (int i = 0; i < ConfigGame.MAX_ENEMIES; i++)
        {
            enemyGO = Instantiate(go);
            enemyGO.name = ValuesInfo.ENEMY_NAME + i;
            enemyBehav = enemyGO.GetComponent<Enemy>();

            enemyBehav.SetEnemyAttrs(player.gameObject.transform.position, player.SubstractLife, enemyDie, i, NotifyEnemyDeadById);
            enemiesList.Add(enemyBehav);
        }

        FillIndexesRandomSpawnPoints();
    }

    private void NotifyEnemyDeadById(int indexEnemy)
    {
        listIndexesEnemiesDead.Add(indexEnemy);
        bool isSpawning = IsInvoking("GenerateEnemy");
        if (!isSpawning)
        {
            indexListEnemiesDead = 0;
            StartSpawn();
        }

    }


    public void StartSpawn()
    {
        InvokeRepeating("GenerateEnemy", 0.1f, ConfigGame.TIME_SPAWN_ENEMIES);
    }


    private void GenerateEnemy()
    {
        if (listIndexesEnemiesDead.Count > 0)
        {
            if (indexListEnemiesDead > listIndexesEnemiesDead.Count - 1)
            {
                indexListEnemiesDead = 0;
            }

            Enemy enemy;
            do
            {
                // Getting the current enemy:
                currentIndexEnemy = listIndexesEnemiesDead[indexListEnemiesDead];
                enemy = enemiesList[currentIndexEnemy];
                ++indexListEnemiesDead;
                if( indexListEnemiesDead > listIndexesEnemiesDead.Count-1)
                {
                    indexListEnemiesDead = 0;
                }
            }
            while (enemy.gameObject.activeInHierarchy);

            // Removing index enemy from dead list
            listIndexesEnemiesDead.Remove(currentIndexEnemy);

            // Set at spawn position
            Vector3 startEnemyPoisition = GetRandomSpawnPosition();
            enemy.SetStartPosition(startEnemyPoisition);
            enemy.EnableColliders(true);
            enemy.gameObject.SetActive(true);
            enemy.EnableNavigation();

            if (enemy.gameObject.activeInHierarchy)
            {
                enemy.GoToTarget();
            }
        }
        else
        {
            CancelInvoke();
        }
    }


    private Vector3 GetRandomSpawnPosition()
    {
        if (randomSpawnPoints.Count == 0)
        {
            FillIndexesRandomSpawnPoints();
        }
        int indexSelected = UnityEngine.Random.Range(0, randomSpawnPoints.Count - 1);
        Vector3 result = randomSpawnPoints[indexSelected];

        randomSpawnPoints.RemoveAt(indexSelected);
        return result;
    }


    public void StopDisableAllEnemies()
    {
        CancelInvoke("GenerateEnemy");
        foreach (Enemy e in enemiesList)
        {
            e.StopAttack();
            e.ResetEnemyLife();
            e.gameObject.SetActive(false);
        }
    }
}
