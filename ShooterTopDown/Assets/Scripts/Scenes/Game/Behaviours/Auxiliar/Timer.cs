﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    private float           countDown;
    private Action<float>   countDownListener;
    private Action          actionFinishTimer;

    public void StartCountdown(float timeSeconds, Action<float> countDownListener, Action actionFinishTimer)
    {
        this.countDown          = timeSeconds;
        this.countDownListener  = countDownListener;
        this.actionFinishTimer  = actionFinishTimer; 

        StartCoroutine(StartCountdownCO());
    }

    private IEnumerator StartCountdownCO()
    {
        while( countDown > 0)
        {
            yield return new WaitForSeconds(1f);
            --countDown;

            if( countDownListener!=null){
                countDownListener.Invoke(countDown);
            }
        }

        if( actionFinishTimer!=null){
            actionFinishTimer.Invoke();
        }
    }

}
