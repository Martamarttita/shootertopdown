﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyLife : MonoBehaviour
{
    private const string ENEMY_LIFE_BAR = "Bar";

    private CanvasGroup cgEnemyLife;
    private Image       enemyLifeBar;

    private bool        enemyPosLoaded;
    private Transform   enemyTransform;

    void Awake()
    {
        enemyPosLoaded = false;

        cgEnemyLife = GetComponent<CanvasGroup>();
        cgEnemyLife.interactable    = false;
        cgEnemyLife.blocksRaycasts  = false;
        

        foreach(Image img in GetComponentsInChildren<Image>())
        {
            if( img.name.Equals(ENEMY_LIFE_BAR)) {
                enemyLifeBar = img;
                break;
            }
        }

        ShowHide(false);
    }

    void Update(){
        if( enemyPosLoaded){
            gameObject.transform.position = enemyTransform.position;
        }
    }

    public void SetEnemyTransform(Transform enemyTransform){
        this.enemyTransform = enemyTransform;
        this.enemyPosLoaded = true;
    }

    public void SetValueLife(float value){
        enemyLifeBar.fillAmount = value;
    }

    public void ShowHide(bool show){
        cgEnemyLife.alpha = show ? 1f : 0f;
    }

}
