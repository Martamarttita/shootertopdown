﻿using System;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    private Vector3         targetPosition;
    private Action<int>     actionAttack;
    private Action<int>     actionEnemyDieScore;
    private Action<int>     actionNotifyEnemyDie;

    private float   currentDistanceTarget;

    private bool    invokeAttack;
    private int     enemyLife;
    private int     idEnemy;

    private NavMeshAgent agent;

    private EnemyLife enemyLifeBehav;

    private SphereCollider collider;

    void Awake()
    {
        agent       = GetComponent<NavMeshAgent>();
        collider    = GetComponent<SphereCollider>();

        gameObject.SetActive(false);

        invokeAttack    = false;
        enemyLife       = ConfigGame.Instance.CurrentMode().EnemyLife;
        enemyLifeBehav  = (Instantiate(Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS + ValuesInfo.ENEMY_LIFE_NAME))).GetComponent<EnemyLife>();
    }


    // Update is called once per frame
    void Update()
    {
        currentDistanceTarget = Vector3.Distance(gameObject.transform.position, targetPosition);
        if (currentDistanceTarget <= ConfigGame.ENEMY_MIN_DIST_DAMAGE*2 && !invokeAttack)
        {
            invokeAttack = true;
            InvokeRepeating("EnemyAttack", 0f, ConfigGame.ENEMY_ATTACK_FREQ);

            if( agent!=null)
            {
                agent.isStopped = true;
                agent.enabled = false;
            }
        }
    }


    public void SetEnemyAttrs(Vector3 targetPosition, Action<int> actionAttack, Action<int> actionEnemyDie, int idEnemy, Action<int> actionNotifyEnemyDie)
    {
        this.targetPosition         = targetPosition;
        this.actionAttack           = actionAttack;
        this.actionEnemyDieScore    = actionEnemyDie;

        this.idEnemy                = idEnemy;
        this.actionNotifyEnemyDie   = actionNotifyEnemyDie;

        this.enemyLifeBehav.name    = ValuesInfo.ENEMY_LIFE_NAME + gameObject.name.Replace(ValuesInfo.ENEMY_NAME, "");
    }

    public void SetStartPosition(Vector3 startPosition)
    {
        if (agent != null){
            agent.Warp(startPosition);
        }
        enemyLifeBehav.SetEnemyTransform(gameObject.transform);
        enemyLifeBehav.ShowHide(true);
    }


    public void GoToTarget()
    {
        if (agent != null){
            agent.SetDestination(targetPosition);
        }
    }


    public bool SubstractEnemyLife(int damage)
    {
        enemyLife -= damage;
        enemyLifeBehav.SetValueLife( (float)enemyLife / (float)ConfigGame.Instance.CurrentMode().EnemyLife );
        bool isDying = enemyLife <= 0;
        if( isDying)
        {
            EnableColliders(false);
        }
        return isDying;
    }

    public void NotifyDeath() {
        if (actionEnemyDieScore != null)
        {
            actionEnemyDieScore.Invoke(ConfigGame.ENEMY_DEATH_POINTS);
        }
        if (actionNotifyEnemyDie != null)
        {
            actionNotifyEnemyDie.Invoke(idEnemy);
        }
    }


    void EnemyAttack()
    {
        if( actionAttack!=null)
        {
            actionAttack.Invoke( ConfigGame.ENEMY_DAMAGE );
        }
    }


    public void StopAttack()
    {
        if (invokeAttack)
        {
            CancelInvoke("EnemyAttack");
        }
    }

    public void ResetEnemyLife()
    {
        enemyLifeBehav.ShowHide(false);
        enemyLifeBehav.SetValueLife(1f);
    }

    public void EnableNavigation()
    {
        if (agent != null)
        {
            agent.enabled = true;
        }
    }


    public void EnableColliders(bool enableValue)
    {
        collider.enabled = enableValue;
    }
}
