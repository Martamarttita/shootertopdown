﻿using System.Collections;
using UnityEngine;


public class BulletBehaviour : MonoBehaviour
{
    private const float MIN_TIME_EXPLOSION = 0.2f;

    private static GameObject particleExplosion;
    private static GameObject particleExplosionEnemy;

    private static bool isLoadParticelSystem = false;

    // Start is called before the first frame update
    void Start()
    {
        // Load once 
        if( !isLoadParticelSystem)
        {
            isLoadParticelSystem = true;
            LoadParticlesSystem();
        }
    }

 
    private static void LoadParticlesSystem()
    {
        particleExplosion       = Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS+ValuesInfo.MUZZLE_FLASH_NAME);
        particleExplosionEnemy  = Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS+ValuesInfo.EXPLOSION_NAME);
    }


    private void OnTriggerEnter(Collider other)
    {
        if( other!=null && other.tag!=null)
        {
            if( other.tag.Equals(ValuesInfo.OBSTACLE_TAG) || other.tag.Equals(ValuesInfo.ENEMY_TAG))
            {
                this.GetComponent<Rigidbody>().isKinematic = true;

                if(other.tag.Equals(ValuesInfo.ENEMY_TAG)){
                    SubstractEnemyLifeOrDie(other);
                }
                StartCoroutine(CreateExplosion(other.tag.Equals(ValuesInfo.OBSTACLE_TAG) ? particleExplosion : particleExplosionEnemy, other));
            }
        }
    }

    private void SubstractEnemyLifeOrDie(Collider other)
    {
        // Substract life from enemy
        Enemy enemyBehaviour = other.gameObject.GetComponent<Enemy>();
        if (enemyBehaviour != null)
        {
            bool isDying = enemyBehaviour.SubstractEnemyLife(ConfigGame.BULLET_DAMAGE);
            if (isDying)
            {
                enemyBehaviour.NotifyDeath();
                enemyBehaviour.StopAttack();
                enemyBehaviour.ResetEnemyLife();
                enemyBehaviour.gameObject.SetActive(false);
            }
        }
    }


    private IEnumerator CreateExplosion(GameObject explosionGO, Collider other)
    {
        // Create and destroy explosion
        GameObject explosion = Instantiate(explosionGO, other.gameObject.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(MIN_TIME_EXPLOSION);
        DestroyImmediate(explosion);

        // Destroy bullet
        DestroyImmediate(gameObject);
    }

    void OnBecameInvisible(){
        Destroy(gameObject);
    }
}
