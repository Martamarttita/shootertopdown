﻿using System;
using System.Collections;
using UnityEngine;

public class Player : MonoBehaviour
{
    private const float     BULLET_FORCE        = 20f;
    private const float     TIME_SHOW_DAMAGE    = 0.1f;
    private const string    MATERIAL_DAMAGE     = "PlayerDamage";

    private Transform   firePoint;
    private GameObject  bulletPrefab;
    
    private int         totalBullets;
    private bool        canShoot;
    private bool        isReloadingAmmo;

    private int         playerLife;

    // Timing window attributes:
    private bool        initTimingWindow;
    private float       startTime;
    private float       currentTime;
    private int         bulletsTiming;

    private Material    matPlayer;
    private Material    matDamage;

    private Action<int> actionShoot;
    private Action<int> actionDamage;
    private Action      actionPlayerDeath;
    private Action      startRechargAmmo;
    private Action      endRechargAmmo;

    [SerializeField]
    private float speedRotation;

    void Awake()
    {
        firePoint           = transform.GetChild(0).transform;
        bulletPrefab        = Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS +ValuesInfo.BULLET_NAME);

        totalBullets        = ConfigGame.MAX_BULLETS;
        isReloadingAmmo     = false;

        playerLife          = ConfigGame.PLAYER_LIFE;

        initTimingWindow    = false;
        bulletsTiming       = 0;

        matPlayer           = gameObject.GetComponent<Renderer>().material;
        matDamage           = Resources.Load<Material>(ValuesInfo.PATH_MATERIALS+MATERIAL_DAMAGE);
    }


    public void SetInitialPositionAndRotation(Vector3 position, float initialRotation)
    {
        gameObject.transform.position = position;
        Vector3 eulerAngles = gameObject.transform.localRotation.eulerAngles;
        eulerAngles.y += initialRotation;
        gameObject.transform.localRotation = Quaternion.Euler(eulerAngles);
    } 

    public void SetRechargeAmmoActions(Action startRechargAmmo, Action endRechargAmmo)
    {
        this.startRechargAmmo   = startRechargAmmo;
        this.endRechargAmmo     = endRechargAmmo;
    }

    public void SetActionDamage(Action<int> actionDamage){
        this.actionDamage = actionDamage;
    }


    public void SetActionPlayerDeath(Action actionPlayerDeath){
        this.actionPlayerDeath = actionPlayerDeath;
    }

    public void SetActionShoot(Action<int> actionShoot){
        this.actionShoot = actionShoot;
    }


    public void RotateLeft(){
        transform.Rotate(0f, 0f, speedRotation * Time.deltaTime);
    }


    public void RotateRight(){
        transform.Rotate(0f, 0f, speedRotation * -Time.deltaTime);
    }


    public void ReloadAmmo(){
        if(!isReloadingAmmo){
            StartCoroutine(ReloadAmmoCO());
        }
    }


    public void Shoot(){
        if (!isReloadingAmmo && totalBullets>0){
            ShootWithTimeWindow();
        }
    }


    public void SubstractLife(int damage)
    {
        if (gameObject.activeSelf)
        {
            StartCoroutine(ChangePlayerAppearance());
        }
        playerLife -= damage;
        if( actionDamage!=null)
        {
            actionDamage.Invoke(playerLife);
        }
        
        if (playerLife <= 0)
        {
            gameObject.SetActive(false);
            if( actionPlayerDeath!=null)
            {
                actionPlayerDeath.Invoke();
            }
        }
    }


    private void ShootWithTimeWindow()
    {
        if( !initTimingWindow)
        {
            initTimingWindow = true;
            startTime = Time.time * 1000;
        }
        currentTime = Time.time * 1000;

        float deltaTime = currentTime - startTime;
        if ( deltaTime< (ConfigGame.TIME_SHOOT * 1000) )
        {
            if( bulletsTiming < ConfigGame.SHOT_BULLETS)
            {
                StartCoroutine(ShootBulletForceCO());
                --totalBullets;
                ++bulletsTiming;
                if( actionShoot!=null)
                {
                    actionShoot.Invoke(totalBullets);
                }
            }
        }
        else
        {
            initTimingWindow    = false;
            bulletsTiming       = 0;
        }
    }


    private IEnumerator ShootBulletForceCO()
    {
        yield return null;
        GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
        Rigidbody rb = bullet.GetComponent<Rigidbody>();
        rb.AddForce(firePoint.up * BULLET_FORCE, ForceMode.Impulse);
    }


    private IEnumerator ReloadAmmoCO()
    {
        isReloadingAmmo = true;

        if ( startRechargAmmo!=null)
            startRechargAmmo.Invoke();

        yield return new WaitForSeconds(ConfigGame.TIME_RECHARGE);
        totalBullets = ConfigGame.MAX_BULLETS;

        if (endRechargAmmo != null)
            endRechargAmmo.Invoke();
        
        isReloadingAmmo = false;
    }


    private IEnumerator ChangePlayerAppearance()
    {
        gameObject.GetComponent<Renderer>().material = matDamage;
        yield return new WaitForSeconds(TIME_SHOW_DAMAGE);
        gameObject.GetComponent<Renderer>().material = matPlayer;
    }

    public int GetCurrentLife()
    {
        return playerLife;
    }
}
