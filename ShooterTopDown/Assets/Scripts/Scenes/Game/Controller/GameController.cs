﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class GameController
{
    private GameView view;

    private Environment     environment;

    private Timer           timer;
    private EnemiesSpawner  enemiesSpawner;
    private InputManager    inputManager;
    private Player          player;

    private int             totalScore;

    public void Init(GameView view)
    {
        this.view = view;
        view.InitComponents(this);
        view.SetPlayerAmmo(ConfigGame.MAX_BULLETS);

        InitEnvironment();

        InitPlayer();
        InitInputManager();
        InitEnemiesSpawner();
        InitTimer();

        totalScore = 0;
    }

    public void GoToStarMenu()
    {
        Navigator.Instance.GoToStartMenu();
    }

    private void InitEnvironment() {
        environment = new Environment(Camera.main);
    }

    private void InitPlayer()
    {
        GameObject playerGO = UnityEngine.Object.Instantiate(Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS + ValuesInfo.PLAYER_NAME));
        playerGO.name = ValuesInfo.PLAYER_NAME;
        player = playerGO.GetComponent<Player>();
        if (player == null){
            player = playerGO.AddComponent<Player>();
        }
        player.SetInitialPositionAndRotation(environment.GetPlayerPoint(), 90f);
        player.SetActionPlayerDeath(FinishGameActions);
        player.SetActionDamage(SetDamageLifePlayerValue);
        player.SetActionShoot((int currentBullets) => view.SetPlayerAmmo(currentBullets));

        player.SetRechargeAmmoActions( 
            () => view.SetPlayerAmmo("Recargando..."), 
            () => view.SetPlayerAmmo(ConfigGame.MAX_BULLETS) 
        );
    }

    private void InitInputManager()
    {
        // Load actions for inputManager
        Dictionary<int, Action> actionsList = new Dictionary<int, Action>()
        {
            { InputManager.LEFT_ARROW_KEY,  player.RotateLeft   },
            { InputManager.RIGHT_ARROW_KEY, player.RotateRight  },
            { InputManager.R_KEY,           player.ReloadAmmo   },
            { InputManager.SPACE_KEY,       player.Shoot        }
        };

        // Create inputManager with actions list:
        inputManager = new GameObject(ValuesInfo.INPUT_MANAGER_NAME).AddComponent<InputManager>();
        inputManager.SetActionsList(actionsList);
    }


    private void InitEnemiesSpawner()
    {
        enemiesSpawner = (new GameObject(EnemiesSpawner.NAME)).AddComponent<EnemiesSpawner>();
        enemiesSpawner.SetAttrs(environment.GetSpawnPoints(),player,(int newMorePoint)=> {
            totalScore += newMorePoint;
            view.SetPlayerScore(totalScore);
        });
        enemiesSpawner.StartSpawn();
    }

    private void InitTimer()
    {
        int initialCountDown = ConfigGame.Instance.CurrentMode().Duration;
        view.ShowTimerValue( ConvertSecondsToFormatTimer(initialCountDown) );

        timer = (new GameObject(ValuesInfo.TIMER_NAME)).AddComponent<Timer>();
        timer.StartCountdown(ConfigGame.Instance.CurrentMode().Duration, (float time) => {
            view.ShowTimerValue(ConvertSecondsToFormatTimer(time));
        }, 
        FinishGameActions);
    }

    private void FinishGameActions()
    {
        // Updating best score throught LocalDataManager if user wins the game
        bool winLose = player.GetCurrentLife() > 0;
        if (winLose && totalScore > LocalDataManager.Instance.MaxScore) { 
            LocalDataManager.Instance.MaxScore = totalScore;
        }

        // Loading information in order to showing in game over panel:
        enemiesSpawner.StopDisableAllEnemies();
        inputManager.EnableInput(false);

        // Showing panel
        view.ShowGameOverPanel(winLose, totalScore);
    }

    private void SetDamageLifePlayerValue(int currentPlayerLife)
    {
        float newValue = 0f;
        if (currentPlayerLife > 0f)
        {
            newValue = ((float)currentPlayerLife / (float)ConfigGame.PLAYER_LIFE);
        }
        view.SetLifeBarValue(newValue);
    }


    private string ConvertSecondsToFormatTimer(float totalSeconds)
    {
        var ss = Convert.ToInt32(totalSeconds % 60).ToString("00");
        var mm = (Math.Floor(totalSeconds / 60) % 60).ToString("00");

        return mm + ":" + ss;
    }

}
