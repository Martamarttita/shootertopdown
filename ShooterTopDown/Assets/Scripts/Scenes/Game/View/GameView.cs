﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GameView : IBaseView
{
    private const string TIMER              = "TimerValue";
    private const string PLAYER_LIFE_BAR    = "LifeBar";
    private const string PLAYER_SCORE       = "Score/Label";
    private const string PLAYER_AMMO        = "Ammo/Label";

    private const string GAME_OVER_PANEL    = "GameOverPanel";
    private const string GAME_OVER_MESSAGE  = GAME_OVER_PANEL+"/Message";
    private const string GAME_OVER_SCORE    = GAME_OVER_PANEL+"/Score";
    private const string GAME_OVER_EXIT_B   = GAME_OVER_PANEL+"/ExitButton";


    private const string WIN_LABEL_TEXT     = "¡Enhorabuena! !Has sobrevivido!";
    private const string LOSE_LABEL_TEXT    = "No has sobrevivido...";

    private GameController controller;

    private Image           lifeBarImage;
    private TextMeshProUGUI playerScore;
    private TextMeshProUGUI playerAmmo;

    private CanvasGroup     cgGameOverPanel;
    private TextMeshProUGUI gameOverLabel;
    private TextMeshProUGUI scoreLabel;
    private TextMeshProUGUI timer;

    public void InitComponents(GameController controller)
    {
        this.controller = controller;
        InitComponents();
    }

    private void InitComponents()
    {
        timer           = GameObject.Find(TIMER).GetComponent<TextMeshProUGUI>();
        lifeBarImage    = GameObject.Find(PLAYER_LIFE_BAR).GetComponent<Image>();
        playerScore     = GameObject.Find(PLAYER_SCORE).GetComponent<TextMeshProUGUI>();
        playerAmmo      = GameObject.Find(PLAYER_AMMO).GetComponent<TextMeshProUGUI>();

        SetLifeBarValue(1f);
        SetPlayerScore(0);

        gameOverLabel   = GameObject.Find(GAME_OVER_MESSAGE).GetComponent<TextMeshProUGUI>();
        scoreLabel      = GameObject.Find(GAME_OVER_SCORE).GetComponent<TextMeshProUGUI>();
        cgGameOverPanel = GameObject.Find(GAME_OVER_PANEL).GetComponent<CanvasGroup>();

        GameObject.Find(GAME_OVER_EXIT_B).GetComponent<Button>().onClick.AddListener( controller.GoToStarMenu );

        ShowCGGameOverPanel(false);
    }

    public void ShowTimerValue(string timerValue)
    {
        timer.text = timerValue;
    }

    private void ShowCGGameOverPanel(bool show)
    {
        cgGameOverPanel.alpha           = show ? 1f : 0f;
        cgGameOverPanel.blocksRaycasts  = show;
        cgGameOverPanel.interactable    = show;
    }

    public void ShowGameOverPanel(bool winLose, int totalScore)
    {
        ShowCGGameOverPanel(true);

        gameOverLabel.text = winLose ? WIN_LABEL_TEXT : LOSE_LABEL_TEXT;
        scoreLabel.text = winLose ? totalScore + " puntos" : "";
    }

    public void SetLifeBarValue(float value)
    {
        lifeBarImage.fillAmount = value;
    }

    public void SetPlayerScore(int score)
    {
        playerScore.text = score.ToString();
    }

    public void SetPlayerAmmo(int ammo)
    {
        playerAmmo.text = ammo == 0 ? "0 => [R]" : ammo.ToString();
    }

    public void SetPlayerAmmo(string value)
    {
        playerAmmo.text = value;
    }
}
