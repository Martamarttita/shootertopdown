﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Environment
{
    public const int OFFSET_Y           = 0;
    public const int OFFSET_Y_CAMERA    = 11;

    public const int BOTTOM_LEFT        = 0;
    public const int TOP_LEFT           = 1;
    public const int BOTTOM_RIGHT       = 2;
    public const int TOP_RIGHT          = 3;

    private Dictionary<int, Vector3> positions;

    private List<Vector3>   spawnPoints;
    private Vector3         playerPoint;

    public Environment(Camera camera)
    {
        InitCameraAttributes(camera);
        InitBasicPositions();

        InitSpawnPointsList(new List<float[]> {
                new float[2]{ 0.5f, 0f },
                new float[2]{ 0.9f, 0f },
                new float[2]{ 0.6f, 1f },
                new float[2]{ 0.9f, 1f },
        });

        InitPlayerPoint(0.3f, 0.5f);
    }

    private void InitCameraAttributes(Camera camera)
    {
        camera.gameObject.transform.position        = new Vector3(0f,OFFSET_Y_CAMERA,0f);
        camera.gameObject.transform.localRotation   = Quaternion.Euler(90, 0, 0);
    }

    private void InitBasicPositions()
    {
        positions = new Dictionary<int, Vector3>();

        CreateEntryDict(BOTTOM_LEFT,    new Vector3(0f,0f,OFFSET_Y_CAMERA));
        CreateEntryDict(BOTTOM_RIGHT,   new Vector3(Camera.main.pixelWidth, 0, OFFSET_Y_CAMERA));
        CreateEntryDict(TOP_LEFT,       new Vector3(0, Camera.main.pixelHeight, OFFSET_Y_CAMERA));
        CreateEntryDict(TOP_RIGHT,      new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, OFFSET_Y_CAMERA));
    }

    private void CreateEntryDict(int key, Vector3 screenPos)
    {
        positions[key] = Camera.main.ScreenToWorldPoint(screenPos);
        Vector3 pos = positions[key];
        pos.y += OFFSET_Y;
        positions[key] = pos;
    }


    public Vector3 GetPosByKey(int key)
    {
        if( positions.ContainsKey(key))
        {
            return positions[key];
        }
        else
        {
            return Vector3.zero;
        }
    }


    public Vector3 GetPosByPercentage(float xPercent, float yPercent)
    {
        if( xPercent<0f || xPercent>1f || yPercent <0f || yPercent>1f)
        {
            return Vector3.zero;
        }
        else
        {
            float width     = Vector3.Distance(positions[TOP_LEFT], positions[TOP_RIGHT]);
            float height    = Vector3.Distance(positions[TOP_RIGHT], positions[BOTTOM_RIGHT]);

            float newWidth  = width * xPercent;
            float newHeight = height * yPercent;

            Vector3 result  = positions[TOP_LEFT];
            result.x        += newWidth;
            result.z        -= newHeight;

            return result;
        }
    }


    private void InitSpawnPointsList( List<float[]> listPoints )
    {
        spawnPoints = new List<Vector3>();

        if( listPoints!=null && listPoints.Count > 0)
        {
            Vector3 posSpawn;
            GameObject pointSpawn;
            Vector3 rotation;
            for(int i=0; i<listPoints.Count; i++)
            {
                posSpawn    = GetPosByPercentage(listPoints[i][0], listPoints[i][1]);
                pointSpawn  = Object.Instantiate(Resources.Load<GameObject>(ValuesInfo.PATH_PREFABS+ValuesInfo.SPAWN_POINT_NAME));

                pointSpawn.name = ValuesInfo.SPAWN_POINT_NAME + i;
                pointSpawn.transform.position = posSpawn;

                if(listPoints[i][1]==0f)
                {
                    rotation = pointSpawn.transform.localEulerAngles;
                    rotation.z = -180;
                    pointSpawn.transform.localEulerAngles = rotation;
                } 

                spawnPoints.Add(posSpawn);
            }
        }
    }


    private void InitPlayerPoint(float xPercent, float yPercent)
    {
        playerPoint = GetPosByPercentage(xPercent, yPercent);
    }


    public List<Vector3> GetSpawnPoints()
    {
        return spawnPoints;
    }

    public Vector3 GetPlayerPoint()
    {
        return playerPoint; 
    }
}
