﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StartMenuView : IBaseView
{
    private const string START_BUTTON           = "StartButton";
    private const string DIFFICULT_TOGGLE       = "DifficultToggle";
    private const string MEDIUM_TOGGLE          = "MediumToggle";

    private const string DURATION_GAME_LABEL    = "DurationGameValue";
    private const string ENEMY_LIFE_LABEL       = "EnemyLifeValue";

    private const string MAX_SCORE_LABEL        = "MaxScoreValue";

    private StartMenuController controller;

    /*private Text durationGame;
    private Text enemyLife;
    private Text maxScore;*/

    private TextMeshProUGUI durationGame;
    private TextMeshProUGUI enemyLife;
    private TextMeshProUGUI maxScore;

    private Toggle defaultToggle;


    public void InitComponents(StartMenuController controller)
    {
        this.controller = controller;
        InitComponents();
    }

    public void ShowModeInfo(ModeGame modeGame)
    {
        durationGame.text   = modeGame.Duration + " segundos";
        enemyLife.text      = modeGame.EnemyLife.ToString();
    }

    public void ShowMaxScore(int value)
    {
        maxScore.text = value.ToString();
    }

    private void InitComponents()
    {
        durationGame    = GameObject.Find(DURATION_GAME_LABEL).GetComponent<TextMeshProUGUI>();
        enemyLife       = GameObject.Find(ENEMY_LIFE_LABEL).GetComponent<TextMeshProUGUI>();
        maxScore        = GameObject.Find(MAX_SCORE_LABEL).GetComponent<TextMeshProUGUI>();

        GameObject.Find(START_BUTTON).GetComponent<Button>().onClick.AddListener( () => controller.StartAction()  );
       
        GameObject.Find(MEDIUM_TOGGLE).GetComponent<Toggle>().onValueChanged.AddListener((bool enable) => ToggleModeAction(false, enable));
        GameObject.Find(DIFFICULT_TOGGLE).GetComponent<Toggle>().onValueChanged.AddListener((bool enable) => ToggleModeAction(true, enable));

        // Medium by default:
        ToggleModeAction(false, true);
    }

    private void ToggleModeAction(bool difficultMedium, bool enable)
    {
        if( enable)
        {
            controller.ChangeMode(difficultMedium);
        }
    }
}
