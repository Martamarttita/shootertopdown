﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuController
{
    private StartMenuView view;

    public void Init(StartMenuView view)
    {
        this.view = view;
        view.InitComponents(this);

        view.ShowMaxScore( LocalDataManager.Instance.MaxScore );
    }

    public void StartAction()
    {
        Navigator.Instance.GoToGame();
    }

    public void ChangeMode(bool difficultMedium)
    {
        view.ShowModeInfo( ConfigGame.Instance.LoadCurrentMode(difficultMedium) );
    }
}
