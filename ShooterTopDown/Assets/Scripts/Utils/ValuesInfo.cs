﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ValuesInfo
{
    // Name in scene
    public const string CANVAS_NAME         = "Canvas";
    public const string TIMER_NAME          = "Timer";
    public const string SPAWN_POINT_NAME    = "SpawnPoint";
    public const string INPUT_MANAGER_NAME  = "InputManager";
    public const string PLAYER_NAME         = "Player";
    public const string BULLET_NAME         = "Bullet";
    public const string OBSTACLE_NAME       = "Asteroid";
    public const string ENEMY_NAME          = "Enemy";
    public const string ENEMY_LIFE_NAME     = "EnemyLife";

    // Tags
    public const string OBSTACLE_TAG        = "ASTEROID";
    public const string ENEMY_TAG           = "ENEMY";

    // Particles
    public const string MUZZLE_FLASH_NAME   = "Muzzle Flash";
    public const string EXPLOSION_NAME      = "ExplosionEnemy";

    public const string PATH_MATERIALS      = "Materials/";
    public const string PATH_PREFABS        = "Prefabs/";
}
