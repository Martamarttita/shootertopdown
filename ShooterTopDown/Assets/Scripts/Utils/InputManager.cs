﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InputManager : MonoBehaviour
{
    public const int LEFT_ARROW_KEY     = 1;
    public const int RIGHT_ARROW_KEY    = 2;
    public const int R_KEY              = 3;
    public const int SPACE_KEY          = 4;

    private Dictionary<int,Action> actionsList;

    private Action          currentAction;
    private List<KeyCode>   keysToCapture;

    private bool            enableInput;

    // Start is called before the first frame update
    void Awake()
    {
        actionsList     = new Dictionary<int, Action>();
        keysToCapture   = new List<KeyCode>();
        enableInput     = true;
    }

    // Update is called once per frame
    private void Update()
    {
        if ( enableInput && keysToCapture != null )
        {
            foreach (KeyCode currentKeyCode in keysToCapture)
            {
                if (Input.GetKey(currentKeyCode))
                {
                    if (actionsList.Count > 0 && actionsList.ContainsKey(ReverseMapKey(currentKeyCode)))
                    {
                        currentAction = actionsList[ReverseMapKey(currentKeyCode)];
                        if (currentAction != null)
                        {
                            currentAction.Invoke();
                        }
                    }
                }
            }
        }
    }

    public void SetActionsList(Dictionary<int, Action> list)
    {
        actionsList     = list;
        keysToCapture   = new List<KeyCode>();

        List<int> keyList = new List<int>(list.Keys);
        foreach( int intKey in keyList)
        {
            keysToCapture.Add(MapKey(intKey));
        }
    }


    public void SetAction(int idKey, Action action)
    {
        if( actionsList!=null && !actionsList.ContainsKey(idKey) )
        {
            actionsList.Add(idKey, action);
            keysToCapture.Add(MapKey(idKey));
        }
    }

    private KeyCode MapKey(int idKey)
    {
        switch (idKey)
        {
            case LEFT_ARROW_KEY:    return KeyCode.LeftArrow;
            case RIGHT_ARROW_KEY:   return KeyCode.RightArrow;
            case R_KEY:             return KeyCode.R;
            case SPACE_KEY:         return KeyCode.Space;

            // Value by default:
            default: return KeyCode.Alpha0;
        }
    }


    private int ReverseMapKey(KeyCode keyCode)
    {
        switch (keyCode)
        {
            case KeyCode.LeftArrow:     return LEFT_ARROW_KEY;
            case KeyCode.RightArrow:    return RIGHT_ARROW_KEY;
            case KeyCode.R:             return R_KEY;
            case KeyCode.Space:         return SPACE_KEY;

            // Value by default:
            default: return -1;
        }
    }

    public void EnableInput(bool value)
    {
        this.enableInput = value;
    }
}
