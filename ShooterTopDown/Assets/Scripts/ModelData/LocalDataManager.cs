﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalDataManager 
{
    public const string KEY_MAX_SCORE = "MaxScore";

    private static LocalDataManager _instance;
    public static LocalDataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new LocalDataManager();
            }

            return _instance;
        }
    }


    private void SetInt(string key, int value)
    {
        PlayerPrefs.SetInt(key, value);
    }

    private int GetInt(string key)
    {
        return PlayerPrefs.GetInt(key);
    }

    public int MaxScore
    {
        get
        {
            return GetInt(KEY_MAX_SCORE);
        }
        set
        {
            SetInt(KEY_MAX_SCORE, value);
        }
    }
}
