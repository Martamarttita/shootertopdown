﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Navigator 
{
    public const string START_MENU = "StartMenu";
    public const string GAME       = "Game";


    private static Navigator _instance;

    public static Navigator Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new Navigator();
            }

            return _instance;
        }
    }
   
    private void LoadNextScene(string nameScene)
    {
        SceneManager.LoadScene(nameScene);
    }


    public void GoToStartMenu()
    {
        LoadNextScene(START_MENU);
    }

    public void GoToGame()
    {
        LoadNextScene(GAME);
    }
}
