﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        switch( SceneManager.GetActiveScene().name)
        {
            case Navigator.START_MENU:
                (new StartMenuController()).Init(new StartMenuView());
                break;

            case Navigator.GAME:
                (new GameController()).Init(new GameView());
                break;
        }
         
    }
}
