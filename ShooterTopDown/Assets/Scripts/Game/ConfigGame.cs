﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigGame 
{
    public const int MAX_ENEMIES = 10;

    // ENEMY ATTRIBUTES:
    // ---------------------------------------------------------------------------
    public const float TIME_SPAWN_ENEMIES   = 2;    // seconds
    public const int ENEMY_SPEED            = 2;    // meters/second
    public const int ENEMY_DAMAGE           = 10;
    public const int ENEMY_DEATH_POINTS     = 5;
    public const int ENEMY_MIN_DIST_DAMAGE  = 1;    // meters
    public const int ENEMY_ATTACK_FREQ      = 1;    // seconds
    // ---------------------------------------------------------------------------

    // PLAYER ATTRIBUTES:
    // ---------------------------------------------------------------------------
    public const int PLAYER_LIFE            = 100;  // 100
    public const int MAX_BULLETS            = 30;
    public const int TIME_RECHARGE          = 3;    // seconds
    public const int SHOT_BULLETS           = 3;    // per second
    public const int BULLET_DAMAGE          = 25;
    public const float TIME_SHOOT           = 1f;
    // ---------------------------------------------------------------------------

    private static ConfigGame _instance;
    public static ConfigGame Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new ConfigGame();
            }

            return _instance;
        }
    }


    private ModeGame _currentMode;
    public ModeGame LoadCurrentMode(bool difficultMedium)
    {
        if( difficultMedium)
        {
            _currentMode = new DifficultModeGame();
        }
        else
        {
            _currentMode = new MediumModeGame();
        }
        return _currentMode;
    }

    public ModeGame CurrentMode()
    {
        return _currentMode;
    }


}
