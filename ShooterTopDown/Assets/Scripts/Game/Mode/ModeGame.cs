﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ModeGame
{
    protected int _duration;
    protected int _enemyLife;

    public int Duration
    {
        get
        {
            return _duration;
        }
    }

    public int EnemyLife
    {
        get
        {
            return _enemyLife;
        }
    }

}
