﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DifficultModeGame : ModeGame
{
    public DifficultModeGame()
    {
        _duration   = 45;
        _enemyLife  = 150;
    }

}
