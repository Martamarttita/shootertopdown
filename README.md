# ShooterTopDown

Juego tipo shooter en tercera persona desarrollado en Unity3D (versión 2019.1.8f1), con vista top-down en el cual debes defenderte
de una oleada de enemigos desde posición estática durante un tiempo determinado.